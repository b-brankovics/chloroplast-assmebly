# Assembling chloroplast genomes

Assembling the chloroplast genome from NGS reads is challenging
because most of them contain an inverted repeat, the two repeat
regions are separated by two non-repeat regions.

**Not possible to resolve the sequence order without longer reads
  (overspanning the repeat), PCR test (overspanning the repeat) or
  restriction data. There has to be data to show the relative
  orientation of the two non-repeat regions to be able to resolve the
  sequence.**

- **Two inverted repeats cannot be resolved if there are two
      non-repeat regions**: It produces two possible solutions
      `1-R-3-R'` or `1-R-3'-R'`

--------------------------
**Nucmer alignment plots of the possible solutions of the repeat resolution.**

| ![self](images/self.png) | ![alternative](images/alternative.png) |
|:------------------------:|:--------------------------------------:|
|**Left:** reference aligned with itself. | **Right:** reference aligned with the alternative sequence solution.|

## Generating _in silico_ NGS reads of circular chloroplast genome

1. The chloroplast genome of _Arabidopsis thailana_ was downloaded
   from NCBI ([NC_000932](reference.fas))

   Structure:
   
   - unique region 1 (1..84170)
   - repeat region (84171..110434)
   - unique region 2 (110435..128214)
   - repeat region invert (128215..154478)


2. The genome is 154478 bp long. The genome was shifted ten times by
   15448 bp using
   [fasta_shift](https://github.com/b-brankovics/grabb/blob/master/helper_programs/fasta_shift)
   from the
   [GRAbB package](https://github.com/b-brankovics/grabb). And from
   each shift 100000 paired end reads were generated with no mutation
   or sequencing errors using [wgsim](https://github.com/lh3/wgsim).
   (See [generate_reads.sh](generate_reads.sh).)
   
        bash generate_reads.sh
        cat sim_?_1.fastq >chl1.fastq
        cat sim_?_2.fastq >chl2.fastq

## Assemblies

3. Trial assembly using SPAdes v.3.6.0.

        spades.py --pe1-1 chl1.fastq.gz --pe1-2 chl2.fastq.gz -k 31,61,91 --only-assembler -o spades

    Assembled into three contigs corresponding to the two non-repeat
    regions and the repeat region with no overlap between them. The
    total assembly size is equal to the length of the reference sequence.

    - NODE_1: matched - 1..84170
	- NODE_2: matched - 84171..110434, matched + 128215..154478
	- NODE_3: matched - 110435..128214


            cp spades/contigs.fasta spades.fas

3. Trial assembly using Edena

        mkdir edena
		cd edena
        edena -paired ../chl1.fastq.gz ../chl2.fastq.gz -p edena
        edena -e edena.ovl -p edena

    Assembled into two contigs:

    - edena_1: 99 bp overlap + first unique region + repeat region
	- edena_2: 99 bp overlap + second unique region + 99 bp overlap (matched + 110336 128313)

            cd ..
            cp edena/edena_contigs.fasta edena.fas

## Identify the repeat region

1. Generate index for mapping

         bwa index edena.fas

2. Map reads to the assembly

		 bwa mem edena.fas chl1.fastq.gz chl2.fastq.gz >bwa_edena.sam
		 
3. Sort the reads in the SAM file

         samtools view -Sb bwa_edena.sam | samtools sort - bwa_edena

4. Scan the output

         samtools view bwa_edena.bam | grep -vP "\t\d+M\t" | cut -f3,4,6 | perl -ne 's/\R//g; if (/^(\S+)\t(\d+)\t(\d+)M\d+S$/) { print "$1\t" . ($2 + $3 - 1) . "\tend\n"} elsif (/^(\S+)\t(\d+)\t\d+S(\d+)M$/) { print "$1\t" . ($2) . "\tstart\n"}' | sort | uniq >test

    1. Open the BAM file
	2. skip lines where the sequence aligned completely
	3. Only use column 3 (contig name), 4 (map position), 5 (map CIGAR)
	4. Perl oneliner to identify starts and ends
	4. Sort the output
	5. Keep only one copy (Maybe `-c` will be good to compare support)

## Using SubRead

1. Generate index file for mapping (**Do not use `-F` option**)

         subread-buildindex -o spades.fas spades.fas
         subread-buildindex -o edena.fas edena.fas
         subread-buildindex -o reference.fas reference.fas

2. Map using SubRead

         subread-align --reportFusions -i spades.fas -r chl1.fastq.gz -R chl2.fastq.gz -o subreadmap_spades_default.sam
         subread-align --reportFusions -i edena.fas -r chl1.fastq.gz -R chl2.fastq.gz -o subreadmap_edena_default.sam
         subread-align --reportFusions -i reference.fas -r chl1.fastq.gz -R chl2.fastq.gz -o subreadmap_reference_default.sam

3. Map using SubJunc

         subjunc --dnaseq  -i spades.fas -r chl1.fastq.gz -R chl2.fastq.gz -o subjuncmap_spades_default.sam
         subjunc --dnaseq  -i edena.fas -r chl1.fastq.gz -R chl2.fastq.gz -o subjuncmap_edena_default.sam
         subjunc --dnaseq  -i reference.fas -r chl1.fastq.gz -R chl2.fastq.gz -o subjuncmap_reference_default.sam

3. Use samtools to sort the reads

         samtools view -Sb subreadmap_edena_default.sam | samtools sort - subreadmap_edena_default
         samtools view -Sb subreadmap_spades_default.sam | samtools sort - subreadmap_spades_default
         samtools view -Sb subreadmap_reference_default.sam | samtools sort - subreadmap_reference_default

4. Index files for visualization

         samtools faidx spades.fas
         samtools faidx edena.fas
         samtools faidx reference.fas

         samtools index subreadmap_spades_default.bam
         samtools index subreadmap_edena_default.bam
         samtools index subreadmap_reference_default.bam

Results:

- neither subread nor subjunc has detected fusion between regions
- BWA result can be used to identify repeat edges

## Proceed after repeat identification

Results:

- Mapping reads to the repeat and _de novo_ assembling reads mapping
  to start or end **has failed**
- Using non-repeat regions as bait for GRAbB generated contigs with
  _sticky ends_ (overlap with the repeat region)

    1. Extract the two contigs
	2. Create two truncated versions of the larger one (first 2/3 and
       second 2/3)
	3. **cat** repeat and **truncated copy 1** and merge
	4. **cat** repeat and **truncated copy 2** and merge
	4. **cat** the two merge results (change entry names if they are
       the same) and merge
	5. **cat** the merge of merges and the shorter contig and merge
	
    Result is a circular contig after trimming.

## Using SPAdes output files to guess sequence and test by mapping

### Process SPAdes output

**If there are two repeat regions (inverted) and two non-repeat
  regions (as in chloroplast genomes).**

1. Read in the FASTG file

    1. [ ] Store the FASTA sequences for each orientation (`/>([^:;]+)/; $id = $1`)
        - mine coverage info
		- compare coverages (identify copy numbers)

		    Divide longer by shorter, get the integer, add 1 and
		    loop 1..(int(l/s)+1) and identify which produces the best
		    fit (Maybe the lowest coverage should be identified first)

	2. [ ] Store the order data for each orientation (if `/>([^:;]+):([^;]+)/`,
	then `$key = $1; @values = split(/,/, $2)`)
	3. [ ] Check whether there is overlap between the neighboring contigs
	(*get_overlap*) (Overlap should not be longer than the longest read)
	    * if yes: create a copy by merging and one by concatenation
          (mapping should test which one is the real case)
		* if no: simply concatenate them

    [ ] **Check for circular layout: if _contig:contig;_ and
	_contig':contig';_ then clip the overlap and mark it circular**

    [ ] **Also check the coverage when there are multiple options for
    continuing**

    Resolvability:

    - Two direct repeats can be resolved
    - **Two inverted repeats cannot be resolved if there are two
      non-repeat regions**: It produces two possible solutions
      `1-R-3-R'` or `1-R-3'-R'`
	- Direct repeats: for **n** repeats, **n-1** solutions
		

2. Map to all versions (remember the joining points)

    1. Check which joining version is correct: higher coverage + indel
       detection result
	2. Check uniform coverage
	3. _Maybe compare multi mapping and random mapping results to
       identify "flattening of the coverage"_

3. Create final output and inform user

Repeats:

- Direct repeats

    * Two repeats and two non-repeats:

        - R:1,2;
		- R:1',2';
		- 1:R;
		- 1':R';
		- 2:R;
		- 2':R';
		- R cov = 1 cov x 2
		- R cov = 2 cov x 2

    * Two repeats and one non-repeat:

        - R:1,R;
		- R:1',R';
		- 1:R;
		- 1':R';
		- R cov = 1 cov x 2

- Inverted repeats 

    * Two repeats and two non-repeats:

        - R:1,1';
		- R:2,2';
		- 1:R';
		- 1':R';
		- 2:R;
		- 2':R;
		- R cov = 1 cov x 2
		- R cov = 2 cov x 2

    * Two repeats and one non-repeat:

        - R:R';
		- R:1,1';
		- 1:R;
		- 1':R;
		- R cov = 1 cov x 2



//
